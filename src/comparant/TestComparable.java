package comparant;

import java.util.ArrayList;
import java.util.Collections;

public class TestComparable
{
	public static void main(String[] args)
	{
		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
		alumnes.add(new Alumne("Valentina", "Pe�a", 21));
		alumnes.add(new Alumne("Albertito", "Bueno", 7));
		alumnes.add(new Alumne("Albertito", "Bueno", 7));
		alumnes.add(new Alumne("Albertito", "Bueno", 18));
		alumnes.add(new Alumne("Albertito", "Malo", 18));
		
		System.out.println(alumnes);
		Collections.sort(alumnes);
		System.out.println(alumnes);
	}
}
