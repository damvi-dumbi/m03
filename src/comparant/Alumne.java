package comparant;

public class Alumne implements Comparable<Alumne>
{
	private String m_name;
	private String m_surname;
	private int m_age;
	
	public Alumne()
	{
		
	}
	
	public String getName()
	{
		return m_name;
	}
	
	public String getSurname()
	{
		return m_surname;
	}
	
	public int getAge()
	{
		return m_age;
	}
	
	public Alumne(String name, String surname, int age)
	{
		m_name = name;
		m_surname = surname;
		m_age = age;
	}

	@Override
	public int compareTo(Alumne other)
	{
		/*
		this < other ---> negatiu
		this > other ---> positiu
		this == other --> 0
		*/
		int surname = m_surname.compareTo(other.getSurname());
		if(surname > 0)
		{
			return 1;
		}else if(surname < 0)
		{
			return -1;
		}else {
			int name = m_name.compareTo(other.getName());
			if(name > 0)
			{
				return 1;
			}else if(name < 0)
			{
				return -1;
			}else {
				if(m_age > other.getAge())
				{
					return 1;
				}else if(m_age < other.getAge())
				{
					return -1;
				}
			}
		}
		return 0;
	}
	
	public String toString()
	{
		return m_surname + ", " + m_name + " - " + m_age;
	}
	
}
