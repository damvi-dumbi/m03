package iterar;

import java.util.ArrayList;
import java.util.Iterator;

public class Test {

	public static void main(String[] args)
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int i=0; i<10; i++)
			list.add(i);
		
		for(int i=0; i<list.size();i++)
			System.out.print(list.get(i));
		
		System.out.println();
		for(Integer obj : list)
			System.out.print(obj);
		
		
		
		System.out.println();
		Iterator<Integer> iterador = list.iterator();
		while(iterador.hasNext())
		{		
			Integer obj = iterador.next();
			System.out.print(obj);
		}
		
		System.out.println();
		System.out.println();
		MyQueue<Integer> queue = new MyQueue<Integer>();
		
		for(int i=0; i<10; i++)
			queue.push(i);
		
		for(Integer integer : queue)
			System.out.print(integer);
	}

}
