package iterar;

import java.util.ArrayList;
import java.util.Iterator;

public class MyQueue<T> implements Iterable<T>
{
	ArrayList<T> list = new ArrayList<T>();

	public void push(T element)
	{
		list.add(element);
	}
	
	private class MyQueueIterator<T> implements Iterator<T>
	{
		private int comptador = -2;
		@Override
		public boolean hasNext()
		{
			//comprovar si hi ha un seg�ent element i tornar
			//true o false
			if((comptador+2)<list.size())
				return true;
			
			return false;
		}

		@Override
		public T next()
		{
			//em mouria a la posici� del seg�ent element i el retornaria
			comptador+=2;
			return (T) list.get(comptador);
		}
		
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return new MyQueueIterator<T>();
	}
}
