package iterar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class Iterant
{
	public static void main(String[] args)
	{
		HashSet<Integer> enters = new HashSet<Integer>();
		for(int i = 0; i < 10; i++)
			enters.add(i);
		System.out.println(enters);
		
		//amb iteradors
		Iterator<Integer> iterator = enters.iterator();
		while(iterator.hasNext())
		{
			Integer enter = iterator.next();
			if(enter == 5)
				iterator.remove();
		}
		
		//sense iteradors fent servir una llista secundaria
		ArrayList<Integer> aBorrar = new ArrayList<Integer>();
		for (Integer enter : enters) {
			if(enter == 8)
				aBorrar.add(enter);
		}
		enters.removeAll(aBorrar);
		
		System.out.println(enters);
	}
}
