package components;

import java.util.ArrayList;

public class EnemicReformed
{
	ArrayList<Comportament> comportaments = new ArrayList<Comportament>();
	
	void addComportament(Comportament comportament)
	{
		comportaments.add(comportament);
	}
	
	void removeComportament(Comportament comportament)
	{
		comportaments.remove(comportament);
	}
	
	void update()
	{
		System.out.println("Comportament d'enemic");
		for (Comportament comportament : comportaments)
		{
			comportament.update();
		}
	}
}
