package components;

import java.util.ArrayList;

public class Test
{
	public static void main(String[] args)
	{
		ArrayList<Enemic> enemics = new ArrayList<Enemic>();
		enemics.add(new Enemic());
		enemics.add(new EnemicVolador());
		enemics.add(new EnemicDeFoc());
		enemics.add(new EnemicVoladorDeFoc());
		enemics.add(new EnemicVoladorDeFoc());
		enemics.add(new EnemicVoladorDeFoc());
		enemics.add(new EnemicVoladorDeFoc());
		
		
		System.out.println("updategem el joc");
		for (Enemic enemic : enemics) {
			enemic.update();
		}
		
		ArrayList<EnemicReformed> trueEnemics = new ArrayList<EnemicReformed>();
		EnemicReformed enemicbase = new EnemicReformed();
		trueEnemics.add(enemicbase);
		enemicbase = new EnemicReformed();
		enemicbase.addComportament(new ComportamentVolador());
		trueEnemics.add(enemicbase);
		enemicbase = new EnemicReformed();
		enemicbase.addComportament(new ComportamentFoc());
		trueEnemics.add(enemicbase);
		enemicbase = new EnemicReformed();
		enemicbase.addComportament(new ComportamentVolador());
		enemicbase.addComportament(new ComportamentFoc());
		trueEnemics.add(enemicbase);
		
		
		
		System.out.println("updategem el joc");
		for (EnemicReformed enemic : trueEnemics) {
			enemic.update();
		}
	}
}

