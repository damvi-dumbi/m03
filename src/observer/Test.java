package observer;

import java.util.ArrayList;

public class Test
{
	public static void main(String[] args)
	{
		Cotxe elMeuCotxe = new Cotxe();
		ArrayList<Observer> palomas = new ArrayList<Observer>();
		
		for(int i=0; i<10; i++)
		{
			Paloma paloma = new Paloma();
			palomas.add(paloma);
			elMeuCotxe.registerObserver(paloma);
			ForoCoche forofo = new ForoCoche();
			palomas.add(forofo);
			elMeuCotxe.registerObserver(forofo);
		}
		
		//passen coses
		elMeuCotxe.netejar();
		
	}
}
