package observer;

import java.util.ArrayList;

public class Observable
{
	ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public void registerObserver(Observer observer)
	{
		observers.add(observer);
	}
	
	public void unregisterObserver(Observer observer)
	{
		observers.remove(observer);
	}

	public void notifyObservers()
	{
		for(Observer observer: observers)
			observer.notifyObserver();
	}
}
