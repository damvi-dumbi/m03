package observer;

public class Paloma implements Observer
{
	private static int id = 0;
	private String nom;
	
	public Paloma()
	{
		nom = "Paloma " + id;
		id++;
	}
	
	public String toString()
	{
		return nom;
	}

	@Override
	public void notifyObserver()
	{
		System.out.println(this + ": -> me vi a caga en ti");		
	}

}
