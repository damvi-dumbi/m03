package observer;

public class ForoCoche implements Observer
{
	private static int id = 0;
	private String nom;
	
	public ForoCoche()
	{
		nom = "ForoCoche " + id;
		id++;
	}
	
	public String toString()
	{
		return nom;
	}
	
	@Override
	public void notifyObserver()
	{
		System.out.println(this + ": -> Pole!");
	}

}
